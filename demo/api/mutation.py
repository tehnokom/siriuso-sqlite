import graphene
import json
from random import randint
from io import StringIO

from django.core.files.base import ContentFile
from django.core.exceptions import ValidationError
from django.db.models import Sum, F

from .schema import SequenceNode, SequenceLineNode
from ..models import *


class CreateLine(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(graphene.String)
    line = graphene.Field(SequenceLineNode)

    class Arguments:
        header_uuid = graphene.UUID(required=True)

    @staticmethod
    def mutate(root, info, header_uuid, **kwargs):
        points_in_line = 500
        status = False
        errors = list()
        header = None
        line = None
        message = 'Ошибка генерации последовательности'

        # Ищем заголовок последовательности
        try:
            header = Header.objects.get(uuid=header_uuid)

            total_points = (
                Line.objects.filter(header=header).aggregate(Sum(F('values_count'))).get('values_count__sum')) or 0

            if not header.completed:
                point_to_generate = min(header.point_count - total_points, points_in_line)
                line = Line(
                    header=header,
                    values_count=point_to_generate,
                    values_json=json.dumps(
                        [randint(header.min_val, header.max_val) for x in range(0, point_to_generate)]
                    )
                )
                line.save()

                if total_points + point_to_generate == header.point_count:
                    header.completed = True
                    header.save()

                status = True
                message = 'Строка сгенерирована'
            else:
                errors.append('Последовательность уже заполнена')

        except Header.DoesNotExist:
            errors.append('Указан неверный заголовок последовательности')

        return CreateLine(status=status, message=message, errors=errors, line=line)


class CreateSequence(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(graphene.String)
    sequence = graphene.Field(SequenceNode)

    class Arguments:
        name = graphene.String(required=True)
        points_count = graphene.Int()
        min_val = graphene.Int()
        max_val = graphene.Int()

    @staticmethod
    def mutate(root, info, name, **kwargs):
        errors = list()

        # Проверяем наличие последовательности с этим именем
        if Header.objects.filter(name=name):
            errors.append("Последовательность с таким именем уже существует")

        if 'points_count' in kwargs \
                and not (MinValueValidator(1)(kwargs.get('points_count')
                         and MaxValueValidator(10000)(kwargs.get('points_count')))
        ):
            errors.append("Число точек последовательности должно быть от 1 до 10'000")

        if 'min_val' in kwargs and not (MinValueValidator(-10000)(kwargs.get('min_val'))
                                        and MaxValueValidator(10000)(kwargs.get('min_val'))
        ):
            errors.append("Нижний порог значений может принимать значения от -10'000 до 10'000")

        if 'max_val' in kwargs and not (MinValueValidator(-10000)(kwargs.get('max_val'))
                                        and MaxValueValidator(10000)(kwargs.get('max_val'))
        ):
            errors.append("Верхний порог значений может принимать значения от -10'000 до 10'000")

        if 'min_val' in kwargs and 'max_val' in kwargs and kwargs.get('min_val') > kwargs.get('max_val'):
            errors.append("Верхний порог значений не может быть меньше нижнего порога значений")

        if not errors:
            header = Header(
                name=name,
                completed=False
            )

            if 'points_count' in kwargs:
                header.point_count = point_count=kwargs.get('points_count', None)

            if 'min_val' in kwargs:
                header.min_val = min_val=kwargs.get('min_val', None)

            if 'max_val' in kwargs:
                header.max_val = kwargs.get('max_val', None)

            header.save()

            CreateLine.mutate(root, info, header.uuid)

            return CreateSequence(status=True, message='Последовательность создана', errors=errors,
                                  sequence=header)

        return CreateSequence(status=False, message='Ошибка(и) значений', errors=errors, sequence=None)


class CreateChartsData(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(graphene.String)
    data = graphene.String()

    class Arguments:
        charts_count = graphene.Int()
        points_count = graphene.Int()
        min_val = graphene.Int()
        max_val = graphene.Int()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        errors = list()
        message = ''
        data = None

        if 'charts_count' in kwargs:
            try:
                MinValueValidator(kwargs.get('charts_count'))
                MaxValueValidator(kwargs.get('charts_count'))
            except ValidationError:
                errors.append('Количество графиков должно быть от 1 до 30')

        if 'points_count' in kwargs:
            try:
                MinValueValidator(kwargs.get('points_count'))
                MaxValueValidator(kwargs.get('points_count'))
            except ValidationError:
                errors.append('Количество точек в графиках должно быть от 500 до 5000')

        if 'min_val' in kwargs:
            try:
                MinValueValidator(kwargs.get('min_val'))
                MaxValueValidator(kwargs.get('min_val'))
            except ValidationError:
                errors.append('Минимальное значение для генерации должно быть от -10\'000 до 10\'000')

        if 'max_val' in kwargs:
            try:
                MinValueValidator(kwargs.get('max_val'))
                MaxValueValidator(kwargs.get('max_val'))
            except ValidationError:
                errors.append('Максимальное значение для генерации должно быть от -10\'000 до 10\'000')

        if not errors:
            data = list()

            for i in range(0, kwargs.get('charts_count', 1)):
                min = kwargs.get('min_val', -10000)
                max = kwargs.get('max_val', 10000)
                data.append([randint(min, max) for x in range(0, kwargs.get('points_count', 5000))])

            out_data = StringIO()
            out_data.write(json.dumps(data))
            json_file = ContentFile(out_data.getvalue())
            json_file.name = 'generated.jdata'
            out_data.close()

            new_item = File(
                file=json_file
            )
            new_item.save()
            status = True
            message = "Файл сгенерирован"
            data = info.context.build_absolute_uri(new_item.file.url)
        else:
            message = 'Есть ошибки в параметрах'

        return CreateChartsData(status=status, message=message, errors=errors, data=data)


class DemoMutations(graphene.ObjectType):
    create_sequence = CreateSequence.Field()
    create_line = CreateLine.Field()
    create_charts_data = CreateChartsData.Field()
