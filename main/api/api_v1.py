import graphene

from demo.api.schema import DemoQuery
from demo.api.mutation import DemoMutations


class Ping(graphene.ObjectType):
    response = graphene.String()

    @staticmethod
    def resolve_response(instance, info):
        return "pong"


class GlobalQuery(DemoQuery):
    ping = graphene.Field(Ping)

    @staticmethod
    def resolve_ping(instance, info):
        return Ping()


class GlobalMutation(DemoMutations):
    pass


schema = graphene.Schema(query=GlobalQuery, mutation=GlobalMutation)
