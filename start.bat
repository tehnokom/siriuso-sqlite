@echo off
TITLE Siriuso-sqlite Server

SET mypath=%~dp0

%mypath:~0,2%
cd %mypath:~0,-1%

For /f "tokens=1-4 delims=/." %%a in ("%DATE%") do (
    SET YYYY=%%c
    SET MM=%%b
    SET DD=%%a
)
For /f "tokens=1-4 delims=/:.," %%a in ("%TIME%") do (
    SET HH24=%%a
    SET MI=%%b
    SET SS=%%c
    SET FF=%%d
)

call venv\Scripts\activate.bat

@echo on

python manage.py migrate > "logs/migration_%DD%-%MM%-%YYYY%-%HH24%_%MI%_%SS%.log"

python server.py > nul 2>"logs/error_%DD%-%MM%-%YYYY%-%HH24%_%MI%_%SS%.log"
pause